<?php
namespace Tests\App\Http\Controllers;

use Laravel\Lumen\Testing\DatabaseMigrations;
use TestCase;

/**
 * Test User Controller
 */
class UserControllerTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * Test creation of user success
     */
    public function testCreateSuccess()
    {
        $data = [
            'name' => 'kyadond',
            'email' => 'timothy@sdd.com',
            'password' => '123'
        ];

        $response = $this->post('/api/v1/user', $data);
        $response->assertResponseStatus(201);
        $response->seeJsonContains([
            'name' => 'kyadond',
            'email' => 'timothy@sdd.com',
            'id' => 1
        ]);
    }

    /**
     * Test invalid fields input
     */
    public function testInvalidInputFields()
    {
        $data = [
            'name' => 'ad3',
            'email' => 'timothy.com',
            'password' => '123'
        ];

        $response = $this->post('/api/v1/user', $data);
        $response->assertResponseStatus(422);
        $response->seeJsonContains([
            "name" => [
                "The name must be at least 5 characters."
            ],
            "email" => [
                "The email must be a valid email address."
            ]
        ]);
    }
}
