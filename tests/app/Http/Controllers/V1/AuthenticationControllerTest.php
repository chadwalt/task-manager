<?php

namespace Tests\App\Http\Controllers;

use Laravel\Lumen\Testing\DatabaseMigrations;
use TestCase;

/**
 * Test AuthenticationController
 */
class AuthenticationController extends TestCase
{
    use DatabaseMigrations;

    /**
     * Test user login with valid credentials
     */
    public function testLogin()
    {
        $user = factory('App\Models\User')->create();
        $response = $this->post('/api/v1/authenticate/login', [
            'email' => $user->email,
            'password' => '123'
        ]);
        $response->assertResponseStatus(200);
        $content = json_decode($response->response->getContent());
        $this->assertEquals(
            ['token', 'token_type', 'expires_in'],
            array_keys(get_object_vars($content))
        );
    }

    /**
     * Test user login with invalid credentials
     */
    public function testInvalidUserCredentials()
    {
        $user = factory('App\Models\User')->create();
        $response = $this->post('/api/v1/authenticate/login', [
            'email' => $user->email,
            'password' => '1223'
        ]);
        $response->assertResponseStatus(401);
        $response->seeJsonContains([
            'error' => 'Invalid Email/Password'
        ]);
    }

    /**
     * Test user logout
     */
    public function testUserLogout()
    {
        $user = factory('App\Models\User')->create();
        $response = $this->actingAs($user)->get('/api/v1/authenticate/logout');
        $response->assertResponseStatus(204);
    }
}
