<?php

namespace Tests\App\Http\Controllers;

use Laravel\Lumen\Testing\DatabaseMigrations;
use TestCase;

/**
 * Test Task Controller
 */
class TaskController extends TestCase
{
    use DatabaseMigrations;
    /**
     * Test successful creation of task
     */
    public function testTaskCreateSuccess()
    {
        $user = factory('App\Models\User')->create();
        $data = [
            'description' => 'Complete reading Advanced PHP Book',
            'completed' => false
        ];

        $response = $this->actingAs($user)->post('/api/v1/task', $data);
        $response->assertResponseStatus(201);
        $response->seeJsonContains([
            'id' => 1,
            'description' => 'Complete reading Advanced PHP Book',
            'completed' => false
        ]);
    }

    /**
     * Test unsuccessful creation of duplicate task
     */
    public function testDuplicateTaskCreation()
    {
        $user = factory('App\Models\User')->create();
        $task = factory('App\Models\Task')->create();
        $response = $this->actingAs($user)->post('/api/v1/task', [
            'description' => $task->description,
            'completed' => $task->completed
        ]);

        $response->assertResponseStatus(409);
        $response->seeJsonContains([
            'message' => $task->description . ' already exists'
        ]);
    }

    /**
     * Test successful task update
     */
    public function testTaskUpdateSuccess()
    {
        $user = factory('App\Models\User')->create();
        $task = factory('App\Models\Task')->create();
        $response = $this->actingAs($user)->patch('/api/v1/task/' . $task->id, [
            'description' => 'Run 100 miles',
            'completed' => false
        ]);

        $response->assertResponseStatus(200);
        $response->seeJsonContains([
            'id' => 1,
            'description' => 'Run 100 miles',
            'completed' => false
        ]);
    }

    /**
     * Test not found on task update
     */
    public function testTaskUpdateNotFound()
    {
        $user = factory('App\Models\User')->create();
        factory('App\Models\Task')->create();
        $response = $this->actingAs($user)->patch('/api/v1/task/2', [
            'description' => 'Run 100 miles',
            'completed' => true
        ]);

        $response->assertResponseStatus(404);
        $response->seeJsonContains([
            'error' => 'Task 2 doesn\'t exist'
        ]);
    }

    /**
     * Test successful task deletion
     */
    public function testTaskDeleteSuccess()
    {
        $user = factory('App\Models\User')->create();
        $task = factory('App\Models\Task')->create();
        $response = $this->actingAs($user)->delete('/api/v1/task/' . $task->id);

        $response->assertResponseStatus(200);
        $response->seeJsonContains([
            'message' => $task->description . ' has been deleted'
        ]);
    }

    /**
     * Test not found on task deletion
     */
    public function testTaskDeleteNotFound()
    {
        $user = factory('App\Models\User')->create();
        factory('App\Models\Task')->create();
        $response = $this->actingAs($user)->delete('/api/v1/task/2');

        $response->assertResponseStatus(404);
        $response->seeJsonContains([
            'error' => 'Task 2 doesn\'t exist'
        ]);
    }

    /**
     * Test task listing
     */
    public function testTaskListingSuccess()
    {
        $user = factory('App\Models\User')->create();
        $tasks = factory('App\Models\Task', 10)->create();
        $response = $this->actingAs($user)->get('/api/v1/task');
        $listings = json_decode($response->response->getContent());

        $response->assertResponseStatus(200);
        $this->assertEquals(count($tasks), count($listings));
        $response->seeJsonContains(get_object_vars($tasks), $listings);
    }
}
