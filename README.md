# Task Manager

Manage your everyday tasks

## Getting Started

Use the following instructions to successfully set up Task Manager API on your computer.

### Prerequisites

To start using this application on your computer you need to have the following:-

- PHP 7.1 or higher
- PostgreSQL
- Composer

### Installing

Clone Task Manager repo by running this command in the terminal

```bash
git clone https://chadwalt@bitbucket.org/chadwalt/task-manager.git
```

Install all composer packages required for the API

```bash
composer install
```

Run all migrations

```bash
php artisan migrate
```

### Generate API Secret

You can generate the API Secret by running this command

```bash
php artisan jwt:secret
```

### Start the Application

To start using the application run the command below, this will start the server. You can specify any port.

```bash
php -S localhost:8000 -t public
```

### Run Test

To run tests use the command below

```bash
composer test
```

## API Endpoints

You can use Postman or curl to hit these endpoints

| Endpoint                   | Verb   | Description                   |
|----------------------------|--------|-------------------------------|
| api/v1/user                | POST   | Create new user by signing up |
| api/v1/authenticate/login  | POST   | Login User                    |
| api/v1/authenticate/logout | GET    | Logout User                   |
| api/v1/task                | POST   | Create Task                   |
| api/v1/task                | PATCH  | Update Task                   |
| api/v1/task                | DELETE | Delete Task                   |
| api/v1/task                | GET    | Get Tasks                     |

