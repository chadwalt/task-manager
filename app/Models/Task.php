<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'description', 'completed', 'user_id'
    ];

    /**
     * Validation Rules
     */
    public static $validationRules = [
        'description' => 'required|string|min:3',
        'completed' => 'required|boolean'
    ];
}
