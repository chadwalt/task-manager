<?php


namespace App\Http\Controllers\V1;

use Illuminate\Http\Request;
use App\Models\Task;
use App\Models\User;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

/**
 * Class TaskController
 *
 * @package App\Http\Controllers
 */

class TaskController extends Controller
{
    /**
     * Create new Task
     *
     * @param Request $request - HTTP Request
     *
     * @return object - Task object
     */
    public function create(Request $request)
    {
        $this->validate($request, Task::$validationRules);

        $user = Auth::user();
        $task = User::find($user->id)->tasks
            ->where('description', $request->description)->first();

        if ($task) {
            return $this->respond(Response::HTTP_CONFLICT, [
                'message' => $request->description . ' already exists'
            ]);
        }

        $task = Task::create([
            'description' => strip_tags($request->description),
            'completed' => $request->completed,
            'user_id' => $user->id
        ]);

        return $this->respond(Response::HTTP_CREATED, $task);
    }

    /**
     * Update task
     *
     * @param Request $request - HTTP Request
     * @param int     $task_id - Task ID
     *
     * @return object - Task object
     */
    public function update(Request $request, int $task_id)
    {
        $this->validate($request, Task::$validationRules);

        $user = Auth::user();
        $task = User::find($user->id)->tasks->where('id', $task_id)->first();

        if (!$task) {
            return $this->respond(Response::HTTP_NOT_FOUND, [
                'error' => 'Task ' . $task_id . ' doesn\'t exist'
            ]);
        }

        $task->description = $request->description;
        $task->completed = $request->completed;
        $task->save();

        return $this->respond(Response::HTTP_OK, $task);
    }

    /**
     * Delete task
     *
     * @param Request $request - HTTP Request
     * @param int     $task_id - Task ID
     *
     * @return object - Task object
     */
    public function delete(Request $request, int $task_id)
    {
        $user = Auth::user();
        $task = User::find($user->id)->tasks->where('id', $task_id)->first();

        if (!$task) {
            return $this->respond(Response::HTTP_NOT_FOUND, [
                'error' => 'Task ' . $task_id . ' doesn\'t exist'
            ]);
        }

        $task->delete();

        return $this->respond(Response::HTTP_OK, [
            'message' => $task->description . ' has been deleted'
        ]);
    }

    /**
     * List tasks
     *
     * @param Request $request - HTTP Request
     *
     * @return object - Task object
     */
    public function index()
    {
        $user = Auth::user();
        $tasks = User::find($user->id)->tasks;

        return $this->respond(Response::HTTP_OK, $tasks);
    }
}
