<?php

namespace App\Http\Controllers\V1;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

/**
 * Class AuthenticateController
 *
 * @package App\Http\Controllers
 */
class AuthenticateController extends Controller
{
    /**
     * AuthenticateController Construct
     */
    public function __construct()
    {
        $this->middleware('auth', [
            'only' => ['logout']
        ]);
    }

    /**
     * Authenticate User
     * 
     * @param Request $request - HTTP Request
     *
     * @return object - Token Object
     */
    public function login(Request $request)
    {
        $credentials = $request->only(['email', 'password']);

        if (!$token = Auth::attempt($credentials)) {
            return $this->respond(Response::HTTP_UNAUTHORIZED, ['error' => 'Invalid Email/Password']);
        }

        return $this->respondWithToken($token);
    }

    /**
     * Logout User
     *
     * @return object - Response Object
     */
    public function logout()
    {
        Auth::logout();
        return $this->respond(Response::HTTP_NO_CONTENT);
    }
}
