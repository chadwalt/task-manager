<?php

namespace App\Http\Controllers\V1;

use Illuminate\Http\Response;
use Laravel\Lumen\Routing\Controller as BaseController;

class Controller extends BaseController
{
    /**
     * Send JSON data
     *
     * @param int   $status - HTTP Response status code
     * @param array $data   - Data to send in the reponse
     */
    protected function respond($status, $data = [])
    {
        return response()->json($data, $status);
    }

    /**
     * Send JWT Token
     *
     * @param string $token - JWT Token
     *
     * @return object JSON JWT Response
     */
    protected function respondWithToken($token)
    {
        return $this->respond(Response::HTTP_OK, [
            'token' => $token,
            'token_type' => 'bearer',
            'expires_in' => Time() + 3600
        ]);
    }
}
