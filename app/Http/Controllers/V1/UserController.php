<?php

namespace App\Http\Controllers\V1;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;

/**
 * Class UserController
 *
 * @package App\Http\Controllers
 */
class UserController extends Controller
{
    /**
     * Create new user
     *
     * @param Request $request - HTTP Request
     *
     * @return object - User object has been created
     */
    public function create(Request $request)
    {
        $this->validate($request, User::$validationRules);

        $user = User::create([
            'name' => strip_tags($request->name),
            'email' => $request->email,
            'password' => strip_tags(Hash::make($request->password))
        ]);
        return $this->respond(Response::HTTP_CREATED, $user);
    }
}
