<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return redirect('https://app.swaggerhub.com/apis/chadwalt/task-manager/1.0.0');
});

/**
 * Routes for Authentication
 */
$router->group(['prefix' => 'api/v1/authenticate'], function () use ($router) {
    $router->post('login', 'AuthenticateController@login');
    $router->get('logout', 'AuthenticateController@logout');
});

/**
 * Routes for tasks
 */
$router->group(['prefix' => 'api/v1/task', 'middleware' => 'auth'], function () use ($router) {
    $router->post('/', 'TaskController@create');
    $router->patch('/{task_id}', 'TaskController@update');
    $router->delete('/{task_id}', 'TaskController@delete');
    $router->get('/', 'TaskController@index');
});

/**
 * Route for user
 */
$router->group(['prefix' => 'api/v1/user'], function () use ($router) {
    $router->post('/', 'UserController@create');
});
